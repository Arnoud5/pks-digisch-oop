<?php
    // error_reporting(E_ALL);
    // ini_set('display_errors', 'On');

    
    require_once ('Frog.php'); 
    require_once ('Ape.php');
    
    
    $sheep = new Animal("shaun");
    echo "Nama Binatang : ".$sheep->name."<br>";
    echo "Jumlah Kaki : ".$sheep->legs."<br>";
    echo "Cold Blooded : ".$sheep->cold_blooded."<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Nama Binatang : ".$sungokong->name."<br>";
    echo "Jumlah Kaki : ".$sungokong->legs."<br>";
    echo "Cold Blooded : ".$sungokong->cold_blooded."<br>";
    echo $sungokong->yell()."<br><br>";

    $kodok = new Frog("buduk");
    echo "Nama Binatang : ".$kodok->name."<br>";
    echo "Jumlah Kaki : ".$kodok->legs."<br>";
    echo "Cold Blooded : ".$kodok->cold_blooded."<br>";
    echo $kodok->jump()."<br><br>";

?>